{config, pkgs, ...}:

{
  # Add vim to installed packages and add some
  # small configuration tweaks
  environment.systemPackages = with pkgs; [
  
    # Customized vim package
    (
      with import <nixpkgs> {};

       vim_configurable.customize {
         name = "vim";
         vimrcConfig.customRC = ''
         syntax on
         set background=dark
         set bs=2
	     set ts=4
	     set expandtab 
         '';
       }
     )

  ];
}
