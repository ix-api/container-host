{lib, ...}: 
let
    webtrigger = fetchTarball {
        url = "https://gitlab.com/ahannig/webtrigger/-/archive/1.1.0/webtrigger-1.1.0.tar.gz";
        sha256 = "04cnfyfhl3ffzpnvzfm3mww6j9xvz5907a1frygzvazbby1rmw8f";
    };
in with lib; 
{
    imports = [
        (import "${webtrigger}/nixos")
    ];

    config.services.webtrigger.enable = true;
}
