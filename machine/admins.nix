{config, ...}:

{
    # This file contains all admin users and their ssh keys
    users.users = {
        annika = {
            isNormalUser = true;
            extraGroups = [ "wheel" ];
            openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyxZyCSZEvgE8689ue2umUGdYvhkqeNYCrEgrgEgqMS annika" ];
        };

        ricardo = {
            isNormalUser = true;
            extraGroups = [ "wheel" ];
            openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC//1rvCIuN6nJbGSUoRbsnPXc7Ljm0CjBMa//dOuBrqXTg3S12og+VMYWQ775z0e59YkkzKJddtMy/QWs9gdJ1yCzElCqVLdi778zG91NwykJCsKN2iRlh2wW33uRQvvoie3Ye6Pl3GurkYlPGOUqGaIFB6NxVdGLqiXZdKPaqQxrrVtEXApQuG8jlkSCLZrEjTkgwhuqgk56G63BSQp+YlMuzAFLPanb7pEucgLBWy4ym1HzY8o2PfdXibjD7SMI92krn0SZ9bZa/Y4eg+wHfJUMF7qVJdb4LmklBOY1+nkHnB4bt9xqcEhClzBXHWSrrxPQc7MvAUZRgdVgWKIun" ];
        };
    };
}
