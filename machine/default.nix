{config, pkgs, ...}: 
{
  imports = [
    ./vim.nix
    ./admins.nix
    ./webtrigger.nix
  ];

  # System configuration: Setup sudo and sudoers
  security.sudo.wheelNeedsPassword = false;
  nix.trustedUsers = [ "@wheel" ];

  # Disable password logins over ssh and configure
  # a nice looking banner.
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
  };

  users.motd = (builtins.readFile ./motd);

  # Enable autoupgrades
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    channel = "https://nixos.org/channels/nixos-20.09";
  };

  # We enable let's encrypt
  security.acme = {
    email = "annika+ixapi-cert@hannig.cc";
    acceptTerms = true;
  };

}
