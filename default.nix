{config, pkgs, ...}: 
{
  imports = [
    ./machine
    ./services
  ];
}
