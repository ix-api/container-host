{config, lib, pkgs, ...}:
let
   cfg           = config.ix-api.services.docs.staging;
   containersCfg = config.virtualisation.oci-containers;
   container     = "docs-staging";

   restartTrigger = pkgs.writeScript "restart" ''
      systemctl restart ${containersCfg.backend}-${container}
   '';
in 
{
    # Options for IX API Docs Service
    options = {
        ix-api.services.docs.staging = {
            enable = lib.mkEnableOption "docs: live service";
            fqdn = lib.mkOption {
              type = lib.types.str;
              description = "the fqdn of the docs live service";
              default = "docs-staging.ix-api.net";
            };
        };
    };

    config = lib.mkIf cfg.enable {

      # Configure container
      virtualisation = {
        oci-containers = { 
          containers = {
            ${container} = {
              image = "registry.gitlab.com/ix-api/ix-api-schema/staging/ix-api-schema:latest";
              cmd = ["serve" "0.0.0.0:8080"];
              ports = ["127.0.0.1:8002:8080"];
              autoStart = true;
            };
          };
        };
      };

      # When restarting, make sure we pull the new image
      # --pull=always is not yet available in this docker version.
      systemd.services."${containersCfg.backend}-${container}".preStart = ''
        ${containersCfg.backend} pull ${containersCfg.containers.${container}.image}
      '';

      # Restart trigger:
      # Enable webtrigger for restarting the container, with sudo
      users.users.restart-docs-staging.isSystemUser = true;
      security.sudo.extraRules = [
        {
            users = [ "restart-docs-staging" ];
            runAs = "root";
            commands = [ 
              { command = "${restartTrigger}";
                options = [ "NOPASSWD" "SETENV" ];
              }
            ];
        }
      ];
        
      services.webtrigger.webtriggers.restart-docs-staging = {
        listen = "127.0.0.1:7201";                              
        pubkey = "fGHLUe32XEttTGkURVvwTxDPVo6yeJ42KMHfyNkS3eA=";
        cmd    = "/run/wrappers/bin/sudo ${restartTrigger}";    
        user   = "restart-docs-staging";                                 
      };

      # Configure nginx vhost
      services.nginx.virtualHosts = {
        ${cfg.fqdn} = {
           forceSSL = true;
           enableACME = true;

           locations."/" = {
             proxyPass = "http://127.0.0.1:8002";
             extraConfig = ''
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $host;
               proxy_set_header X-Forwarded-Proto $scheme;
             '';
           };

           locations."/_webtrigger/restart" = {
             proxyPass = "http://127.0.0.1:7201";
           };
        };
      };
    };
}
