{config, lib, pkgs, ...}:
let
   cfg           = config.ix-api.services.docs.live;
   containersCfg = config.virtualisation.oci-containers;
   container     = "docs-live";
  
   restartTrigger = pkgs.writeScript "restart" ''
     systemctl restart ${containersCfg.backend}-${container}
   '';
in 
{
    # Options for IX API Docs Service
    options = {
        ix-api.services.docs.live = {
            enable = lib.mkEnableOption "docs: live service";
            fqdn = lib.mkOption {
              type = lib.types.str;
              description = "the fqdn of the docs live service";
              default = "docs.ix-api.net";
            };
            errors-fqdn = lib.mkOption {
              type = lib.types.str;
              description = "the fqdn of the docs live service";
              default = "errors.ix-api.net";
            };
        };
    };

    config = lib.mkIf cfg.enable {

      # Configure container
      virtualisation = {
        oci-containers = { 
          containers.${container} = {
            image = "registry.gitlab.com/ix-api/ix-api-schema/live/ix-api-schema:latest";
            cmd = ["serve" "0.0.0.0:8080"];
            ports = [ "127.0.0.1:8001:8080"];
            autoStart = true;
          };
        };
      };

      # When restarting, make sure we pull the new image
      # --pull=always is not yet available in this docker version.
      systemd.services."${containersCfg.backend}-${container}".preStart = ''
        ${containersCfg.backend} pull ${containersCfg.containers.${container}.image}
      '';

      # Restart trigger
      users.users.restart-docs-live.isSystemUser = true;
      security.sudo.extraRules = [ {
        users = [ "restart-docs-live" ];
        runAs = "root";
        commands = [ {
          command = "${restartTrigger}";
          options = [ "NOPASSWD" "SETENV" ];
        } ];
      } ];
    
      services.webtrigger.webtriggers.restart-docs-live = {
        listen = "127.0.0.1:7202";
        pubkey = "fGHLUe32XEttTGkURVvwTxDPVo6yeJ42KMHfyNkS3eA=";
        cmd    = "/run/wrappers/bin/sudo ${restartTrigger}";
        user   = "restart-docs-live";
      };

      # Configure nginx vhost
      services.nginx.virtualHosts = {
        # Redoc / API docs
        ${cfg.fqdn} = {
           forceSSL = true;
           enableACME = true;

           locations."/" = {
             proxyPass = "http://127.0.0.1:8001";
             extraConfig = ''
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $host;
               proxy_set_header X-Forwarded-Proto $scheme;
             '';
           };

           locations."/_webtrigger/restart" = {
             proxyPass = "http://127.0.0.1:7202";
           };
        };

        # Errors
        ${cfg.errors-fqdn} = {
           forceSSL = true;
           enableACME = true;

           locations = {
             "/" = {
               proxyPass = "http://127.0.0.1:8001";
               extraConfig = ''
                 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                 proxy_set_header Host $host;
                 proxy_set_header X-Forwarded-Proto $scheme;
               '';
            };
            "= /" = {
              extraConfig = ''
                return 302 /v2/problems;
              '';
            }; 
          }; 

        };

      };
    };
}
