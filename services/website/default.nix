{config, pkgs, lib, ...}:
let
  cfg = config.ix-api.services.website;
  containersCfg = config.virtualisation.oci-containers;
  
  # Configuration
  options = {
    enable = lib.mkEnableOption "host the public website";
    fqdn = with lib; mkOption {
      type = types.str;
      description = "the fqdn of the website";
      default = "ix-api.net";
    };
  };

in
{
  options.ix-api.services.website = options;

  # Implementation
  config = lib.mkIf cfg.enable {
      # Configure container
      virtualisation = {
        oci-containers = { 
          containers."ix-api-website" = {
            image = "registry.gitlab.com/ix-api/ix-api-website/live/ix-api-website:latest";
            ports = ["127.0.0.1:9001:80"];
            autoStart = true;
          };
        };
      };

      # When restarting, make sure we pull the new image
      # --pull=always is not yet available in this docker version.
      systemd.services."${containersCfg.backend}-ix-api-website".preStart = ''
        ${containersCfg.backend} pull ${containersCfg.containers."ix-api-website".image}
      '';
      
      # Configure nginx
      services.nginx.virtualHosts.${cfg.fqdn} = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:9001";
          extraConfig = ''
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-Proto $scheme;
          '';
        };
      };
  };
}
