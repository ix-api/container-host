{
  networking = { hostName = "container1"; domain = "infra.ix-api.net"; };

  # IX-API containers
  ix-api.services = {

    # Docs
    docs.live = { 
      enable      = true;
      fqdn        = "docs.unmodulatedcarrier.net";
      errors-fqdn = "errors.unmodulatedcarrier.net";
    };
    docs.staging = { 
      enable     = true;
      fqdn       = "docs-staging.unmodulatedcarrier.net";
    };

    # Website
    website = {
      enable     = true;
      fqdn       = "www-staging.ix-api.net";
    };
  };
}
